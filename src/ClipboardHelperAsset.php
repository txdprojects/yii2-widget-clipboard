<?php

namespace txd\widgets\clipboard;

use yii\web\AssetBundle;

class ClipboardHelperAsset extends AssetBundle
{
	/**
	 * @inheritdoc
	 */
	public $css = [

	];

	/**
	 * @inheritdoc
	 */
	public $js = [
		'js/yii.clipboard.js',
	];

	/**
	 * @inheritdoc
	 */
	public $depends = [
		'txd\widgets\clipboard\ClipboardAsset',
	];

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		$this->sourcePath = __DIR__ . '/assets';
	}
}
