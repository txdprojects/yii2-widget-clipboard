<?php

namespace txd\widgets\clipboard;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\InputWidget;

/**
 * Class Clipboard
 *
 * @link https://github.com/zenorocha/clipboard.js
 *
 * @author TUXIDO <hello@tuxido.ro>
 */
class Clipboard extends InputWidget
{
	const INPUT_ADDON_PREPEND = 'prepend';
	const INPUT_ADDON_APPEND = 'append';

	/**
	 * @var bool|string The input addon
	 */
	public $inputAddon = self::INPUT_ADDON_PREPEND;

	/**
	 * @var string|null The input addon content
	 */
	public $inputAddonContent;

	/**
	 * @var array The container options
	 */
	public $containerOptions = [];

	/**
	 * @var array The client (JS) options
	 */
	public $clientOptions = [];

	/**
	 * @var array The client (JS) events
	 */
	public $clientEvents = [];

	/**
	 * @var string The client (JS) selector
	 */
	private $_clientSelector;

	/**
	 * @var string The global widget JS hash variable
	 */
	private $_hashVar;


	/**
	 * {@inheritdoc}
	 */
	public function init()
	{
		parent::init();

		$this->setupProperties();
		$this->registerAssets();
	}

	/**
	 * {@inheritdoc}
	 */
	public function run()
	{
		$content = [];
		$content[] = Html::beginTag('div', $this->containerOptions);
		$content[] = $this->renderInputHtml('text');
		$content[] = Html::endTag('div');

		if ($this->inputAddon) {
			if ($this->inputAddon === self::INPUT_ADDON_PREPEND) {
				array_splice($content, 1, 0, $this->renderInputAddon(self::INPUT_ADDON_PREPEND));
			} else {
				array_splice($content, 2, 0, $this->renderInputAddon(self::INPUT_ADDON_APPEND));
			}
		}

		return implode("\n", $content);
	}

	/**
	 * Gets the client selector.
	 *
	 * @return string
	 */
	public function getClientSelector()
	{
		if (!$this->_clientSelector) {
			$this->_clientSelector = '#' . $this->getId();
		}
		return $this->_clientSelector;
	}

	/**
	 * Gets the hash variable.
	 *
	 * @return string
	 */
	public function getHashVar()
	{
		if (!$this->_hashVar) {
			$this->_hashVar = 'clipboard_' . hash('crc32', $this->buildClientOptions());
		}
		return $this->_hashVar;
	}

	/**
	 * Sets the widget properties.
	 */
	protected function setupProperties()
	{
		if ($this->hasModel()) {
			$this->setId(Html::getInputId($this->model, $this->attribute) . '-clipboard');
		} else {
			$this->options['id'] = "{$this->getId()}-form-control";
		}

		// Use the container ID for widget if is set
		if (isset($this->containerOptions['id'])) {
			$this->setId($this->containerOptions['id']);
		} else {
			$this->containerOptions['id'] = $this->getId();
		}

		$this->options = ArrayHelper::merge([
			'class' => 'form-control',
			'autocomplete' => 'off',
			'data' => [
				'clipboard-options' => $this->getHashVar(),
			],
		], $this->options);

		if ($this->inputAddon) {
			Html::addCssClass($this->containerOptions, 'input-group');
			if (!$this->inputAddonContent) {
				$this->inputAddonContent = Html::button('<span class="fal fa-copy"></span>', [
					'class' => 'btn btn-outline-secondary',
					'data' => [
						'clipboard-target' => "#{$this->options['id']}",
					],
				]);
			}
		}

		Html::addCssClass($this->containerOptions, 'clipboard-container');
		Html::addCssClass($this->options, 'clipboard-input');
	}

	/**
	 * Builds the client options.
	 *
	 * @return string
	 */
	protected function buildClientOptions()
	{
		$defaultClientOptions = [

		];

		$clientOptions = ArrayHelper::merge($defaultClientOptions, $this->clientOptions);

		return Json::encode($clientOptions);
	}

	/**
	 * Registers the widget assets.
	 */
	protected function registerAssets()
	{
		// Get the view
		$view = $this->getView();

		// Register assets
		ClipboardAsset::register($view);
		ClipboardHelperAsset::register($view);

		// Register widget hash JavaScript variable
		$view->registerJs("var {$this->getHashVar()} = {$this->buildClientOptions()};", View::POS_HEAD);

		// Build client script
		$js = "jQuery('{$this->getClientSelector()}').yiiClipboard({$this->getHashVar()})";

		// Build client events
		if (!empty($this->clientEvents)) {
			foreach ($this->clientEvents as $clientEvent => $eventHandler) {
				if (!($eventHandler instanceof JsExpression)) {
					$eventHandler = new JsExpression($eventHandler);
				}
				$js .= ".on('{$clientEvent}', {$eventHandler})";
			}
		}

		// Register widget JavaScript
		$view->registerJs("{$js};");
	}

	/**
	 * Renders a Bootstrap input group addon.
	 *
	 * @param string $position
	 * @return string
	 */
	protected function renderInputAddon($position = self::INPUT_ADDON_PREPEND)
	{
		return Html::tag('div', $this->inputAddonContent, [
			'class' => "input-group-{$position}",
		]);
	}
}
